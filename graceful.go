package graceful

import (
	"context"
	"errors"
	"sync"
	"time"
)

var (
	// ErrProgramClosed 程序已经退出
	ErrProgramClosed = errors.New("the program has been closed")

	//ErrTimeoutExit 超时退出
	ErrTimeoutExit = errors.New("timeout exit")
)

// Graceful 优雅退出库
// 此库所有的操作都是并发安全的
// sync.WaitGroup 虽然可以并发控制但是，自己本身不是并发安全的，并发add wait就会出现数据竞争，所有WaitGroup不适合做优雅退出
type Graceful struct {
	ctx       context.Context
	ctxCancel context.CancelFunc
	ctxLock   sync.Mutex

	num     int64
	numLock sync.Mutex
}

func (s *Graceful) Add(delta int64) {
	s.numLock.Lock()
	defer s.numLock.Unlock()
	s.num += delta
}

func (s *Graceful) Done() {
	s.numLock.Lock()
	defer s.numLock.Unlock()
	s.num--
}

func (s *Graceful) initCtx() {
	s.ctxLock.Lock()
	defer s.ctxLock.Unlock()
	if s.ctx == nil {
		s.ctx, s.ctxCancel = context.WithCancel(context.Background())
	}
}

// Ctx 返回context.Context,可过监听 <-ctx.Done()来判断服务器是否关闭
func (s *Graceful) Ctx() context.Context {
	s.initCtx()
	return s.ctx
}

// IsStop true关闭 false未关闭
func (s *Graceful) IsStop() bool {
	s.initCtx()
	select {
	case <-s.Ctx().Done():
		return true
	default:
		return false
	}
}

// Num 获得正在运行的goroutine个数
func (s *Graceful) Num() int64 {
	s.numLock.Lock()
	defer s.numLock.Unlock()
	return s.num
}

// Stop 停止服务,可重复调用
func (s *Graceful) Stop() {
	s.initCtx()
	s.ctxCancel()
}

// WaitExit 停止并阻塞等待所有 goroutine 结束
func (s *Graceful) WaitExit(t time.Duration) error {
	s.Stop()
	endTime := time.Now().Add(t)
	for {
		if d, err := s.waitExit(endTime); err != nil {
			return err
		} else if d == 0 {
			return nil
		} else {
			time.Sleep(d)
		}
	}
}

func (s *Graceful) waitExit(timeout time.Time) (waitDuration time.Duration, err error) {
	s.numLock.Lock()
	defer s.numLock.Unlock()

	//检测是否等待超时
	if time.Now().After(timeout) == true {
		return 0, ErrTimeoutExit
	}

	//检测是否所有的goroutine都已经关闭
	if s.num == 0 {
		return 0, nil
	} else if s.num < 0 {
		return 0, errors.New("graceful.num cannot be less than 0") //小于0说明用法有问题
	}

	//计算下次等待时长
	d := time.Millisecond
	if s.num > 1000 {
		d = d * 1000 //最大1秒
	} else if s.num < 100 {
		d = d * 100 //最小100毫秒
	} else {
		d = d * time.Duration(s.num)
	}
	return d, nil
}
