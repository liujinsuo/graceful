package main

import (
	"fmt"
	"gitee.com/liujinsuo/graceful"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	one()
}

// 用法1 单个服务
func one() {
	gs := graceful.Graceful{}
	for i := 0; i < 10; i++ {
		go func(i int) {
			if gs.IsStop() == true {
				fmt.Println("程序已经关闭")
				return
			}
			gs.Add(1)
			defer gs.Done()
			time.Sleep(time.Millisecond * 1 * time.Duration(i))
			fmt.Println(i)

			//gs.Stop()
		}(i)
	}

	go func() {
		fmt.Println("阻塞等待服务关闭")
		<-gs.Ctx().Done()
		fmt.Println("服务关闭成功")
	}()

	quit := make(chan os.Signal)
	fmt.Println("监控信号")
	signal.Notify(quit, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGINT, syscall.SIGTERM)
	sig := <-quit
	fmt.Println("收到信息", sig)

	fmt.Println("等待程序结束")
	//time.Sleep(time.Millisecond)

	gs.Stop()
	gs.Stop()

	//阻塞等待所有goroutine结束
	if err := gs.WaitExit(time.Second * 5); err != nil {
		fmt.Println("非优雅关闭", gs.Num(), err)
	} else {
		fmt.Println("优雅关闭成功", gs.Num())
	}
}
